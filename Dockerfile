ARG BASE_REGISTRY=registry1.dso.mil/ironbank
ARG BASE_IMAGE=redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG} as extract

COPY git-lfs-linux-amd64-v3.1.2.tar.gz /
RUN mkdir -p /opt/application && \
  tar -zxf /git-lfs-linux-amd64-v3.1.2.tar.gz -C /opt/application

FROM bitnami/git:2.35.1 as base
RUN rm /opt/bitnami/common/bin/gosu

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

ARG BITNAMI_HOME=/opt/bitnami
ENV HOME=/home/git

ARG UID=1000
ARG GID=1000

COPY --from=base ${BITNAMI_HOME} ${BITNAMI_HOME}
COPY --from=extract /opt/application/git-lfs /usr/bin/
COPY scripts ${BITNAMI_HOME}/scripts

# OpenSCAP RuleID xccdf_org.ssgproject.content_rule_file_permissions_binary_dirs
RUN chown -R $UID:$GID ${BITNAMI_HOME}
RUN chmod -R go-w ${BITNAMI_HOME}

# Install required system packages and dependencies
RUN dnf update -y --nodocs && \
  dnf clean all && \
  rm -rf /var/cache/dnf

RUN mkdir $HOME && \
  chown -R $UID:$GID $HOME && \
  chown root:root /usr/bin/git-lfs && \
  chown -R $UID:$GID ${BITNAMI_HOME}

WORKDIR $HOME

ENV BITNAMI_APP_NAME="git" \
  BITNAMI_IMAGE_VERSION="2.35.1" \
  NSS_WRAPPER_LIB="/opt/bitnami/common/lib/libnss_wrapper.so" \
  PATH="/opt/bitnami/common/bin:/opt/bitnami/git/bin:$PATH"

USER $UID:$GID
RUN git lfs install

HEALTHCHECK --start-period=10s CMD bash -c 'git --version && git lfs --version' || exit 1

ENTRYPOINT [ "/opt/bitnami/scripts/git/entrypoint.sh" ]
CMD [ "/bin/bash" ]
